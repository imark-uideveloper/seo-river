$('.client-logo-slider').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots:false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$('.testimonial-slider').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    dots:false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});


wow = new WOW({
    mobile: false, // default
})
wow.init();
